# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog][],
and this project adheres to [Semantic Versioning][].

## [1.3.1][] — 2019-09-28

### Added

-   HTML minification.

### Fixed

-   HTTP2 push.

## [1.3.0][] — 2019-09-24

### Added

-   Sitespeed.io metrics.
-   Censor review.
-   Merge request templates for bugfix and feature.

### Fixed

-   Semver.

### Changed

-   Replaced Graphik with IBM Plex Sans.
-   Replaced static font with npm version.

## [1.2.0][] — 2019-09-15

### Added

-   Build tagged commits.
-   Service worker.
-   Web Manifest.
-   Font swap logic.
-   Dynamic `theme-color`.

### Changed

-   CSS transormed to SCSS.

## [1.1.0][] — 2019-09-15

### Added

-   English pages.
-   Pug linting.
-   16×16 favicon.
-   Nginx language redirect.

## [1.0.0][] - 2019-09-14

### Added

-   Basic desktop and mobile layout.
-   Build system.
-   CI/CD.
-   Only russian language.
-   Added basic lints.
-   Forced code style.
-   Hooks.

[keep a changelog]: https://keepachangelog.com/en/1.0.0/

[semantic versioning]: https://semver.org/spec/v2.0.0.html

[1.0.0]: https://gitlab.com/mishamyrt/myrt.co/tree/v1.0.0

[1.1.0]: https://gitlab.com/mishamyrt/myrt.co/tree/v1.1.0

[1.2.0]: https://gitlab.com/mishamyrt/myrt.co/tree/v1.2.0

[1.3.0]: https://gitlab.com/mishamyrt/myrt.co/tree/v1.3.0

[1.3.1]: https://gitlab.com/mishamyrt/myrt.co/tree/v1.3.1
